import { Footer } from "./components";

import {
  NavbarUI,
  HeroUI,
  SignUpUI,
  SignInUI,
  SubjectsUI,
  WhyUsUI,
  OurPartnersUI,
  ContactUsUI,
} from "./features";

function App() {
  return (
    <div dir="rtl" className="w-full">
      <NavbarUI />

      {/* Auth Section*/}
      <SignUpUI />
      <SignInUI />

      {/* Hero Section */}
      <HeroUI />

      {/* Subjects Section */}
      <SubjectsUI />

      {/* Why Us Section */}
      <WhyUsUI />

      {/* Our Partners Section*/}
      <OurPartnersUI />

      {/* Contact Us Section */}
      <ContactUsUI />

      <Footer />
    </div>
  );
}

export default App;
