import { configureStore, getDefaultMiddleware } from "@reduxjs/toolkit";
import signupReducer from "../features/auth/sign-up/sign-up.slice";
import signinReducer from "../features/auth/sign-in/sign-in.slice";

import { apiSlice } from "../features/api/api.slice";

const store = configureStore({
  reducer: {
    signup: signupReducer,
    signin: signinReducer,
    [apiSlice.reducerPath]: apiSlice.reducer,
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware().concat(apiSlice.middleware),
});

export default store;

// Infer the `RootState` and `AppDispatch` types from the store itself
export type RootState = ReturnType<typeof store.getState>;
// Inferred type: {posts: PostsState, comments: CommentsState, users: UsersState}
export type AppDispatch = typeof store.dispatch;
