import React from "react";
import Button from "./button.component";

import AppleIcon from "../../assets/icons/apple.svg";

const AppleStoreComponent: React.FC = () => {
  return (
    <>
      <Button
        type={"button"}
        bgColor={"bg-black"}
        color={"text-white"}
        className="gap-2"
        padding="md:p-2.5 md:py-3 py-1 px-2 "
      >
        <div className="flex flex-col items-end">
          <span className="md:text-[12px] text-[8px]">Download on the</span>
          <span className="md:text-lg text-md">App Store</span>
        </div>
        <img src={AppleIcon} alt="AppleIcon" className="md:h-10 h-6" />
      </Button>
    </>
  );
};

export default AppleStoreComponent;
