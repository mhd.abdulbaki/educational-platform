import React, { ReactNode } from "react";

interface Props {
  type: "button" | "submit";
  bgColor?: string;
  color: string;
  children?: ReactNode | string;
  className?: string | undefined;
  padding?: string;
  onClick?: () => {} | unknown;
}

const Button: React.FC<Props> = ({
  children,
  type = "button",
  bgColor,
  color = "text-slate-900 hover:text-blue-600",
  className,
  padding = "md:p-2.5 py-3 px-2 ",
  onClick,
}) => {
  return (
    <button
      onClick={onClick}
      type={type}
      className={`flex justify-center items-center  transition-all duration-300 ${padding} ease-in-out delay-0  general-button rounded-md ${bgColor} ${color} font-bold ${className}`}
    >
      {children}
    </button>
  );
};

export default Button;
