import React from "react";
import Button from "./button.component";
import GoogleIcon from "../../assets/icons/google-play.svg";

const GooglePlayComponent: React.FC = () => {
  return (
    <>
      <Button
        type={"button"}
        bgColor={"bg-black"}
        color={"text-white"}
        className="gap-2"
        padding="md:p-2.5 md:py-3 py-1 px-2 "
      >
        <div className="flex flex-col items-end">
          <span className="md:text-[11px] text-[8px]">GET IT On</span>
          <span className="md:text-xl text-md">Google Play</span>
        </div>
        <img src={GoogleIcon} alt="Google Icon" className="md:h-10 h-6" />
      </Button>
    </>
  );
};

export default GooglePlayComponent;
