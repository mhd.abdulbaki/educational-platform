import React from "react";

import { Typography, Heading, Wrapper } from "..";

interface CardComponentInterface {
  imageSrc: string;
  imageAlt: string;
  title: string;
  des: string;
  stars: number;
  reviews: number;
  points: number;
}

const CardComponent: React.FC<CardComponentInterface> = ({
  imageAlt,
  imageSrc,
  des,
  stars,
  reviews,
  points,
  title,
}) => {
  return (
    <>
      <div className="max-w-sm bg-white border border-gray-100 rounded-lg shadow">
        <img className="rounded-t-lg" src={imageSrc} alt={imageAlt} />

        <Wrapper className="px-10 py-2">
          <Heading
            fontSize="text-[18px]"
            fontWeight="font-bold"
            className="mb-2"
          >
            {title}
          </Heading>

          <Typography
            fontSize="text-[16px]"
            color="text-gray-700"
            className="mb-3 font-normal "
          >
            {des}
          </Typography>
        </Wrapper>

        <Wrapper className="px-10 py-2">
          <div className="flex items-center mt-2.5 mb-5">
            <svg
              aria-hidden="true"
              className="w-8 h-8 text-yellow-300"
              fill="currentColor"
              viewBox="0 0 20 20"
              xmlns="http://www.w3.org/2000/svg"
            >
              <title>First star</title>
              <path d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z"></path>
            </svg>

            <span className=" text-slate-700 text-[16px] font-bold mr-2 py-0.5 rounded  ml-3">
              {stars}
            </span>
            <span className=" text-slate-700 text-[16px] mr-2 py-0.5 rounded  ml-3">
              ({reviews} Reviews)
            </span>

            <a
              href="/#"
              className=" text-[#0056D2] text-sm font-semibold underline mr-2 py-0.5 rounded  ml-3 cursor-pointer"
            >
              {points} نقطة
            </a>
          </div>
        </Wrapper>
      </div>
    </>
  );
};

export default CardComponent;
