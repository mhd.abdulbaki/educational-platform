import React, { ReactNode } from "react";
import { Swiper } from "swiper/react";

//  Swiper styles
import "swiper/css";
import "swiper/css/navigation";

// required modules
import { Navigation, Pagination, Keyboard } from "swiper";

interface CarouselComponentInterface {
  children: ReactNode;
  breakpoints: {};
  pagination:
    | boolean
    | {
        clickable: boolean;
      };
}

const CarouselComponent: React.FC<CarouselComponentInterface> = ({
  children,
  pagination,
  breakpoints,
}) => {
  return (
    <>
      <Swiper
        // slidesPerView={slidesPerView}
        spaceBetween={20}
        navigation={true}
        pagination={pagination}
        keyboard={true}
        modules={[Navigation, Pagination, Keyboard]}
        className="mySwiper"
        breakpoints={breakpoints}
      >
        {children}
      </Swiper>
    </>
  );
};

export default CarouselComponent;
