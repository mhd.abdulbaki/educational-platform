import React from "react";
import perlaLogo from "../../assets/perla-logo.png";

const Footer: React.FC = () => {
  return (
    <div className="max-w-screen-3xl w-full flex lg:flex-row flex-col flex-wrap items-center justify-between mx-auto p-10 gap-4 lg:gap-0">
      <div className="flex gap-4 items-center">
        <a href="/#" className="text-[16px] underline text-slate-900">
          شروط الاستخدام
        </a>
        <a href="/#" className="text-[16px] underline text-slate-900">
          سياسة الخصوصية
        </a>
      </div>

      <div className="flex gap-4 items-center">
        <p className="text-[16px] text-slate-900 text-center">
          © 2023 منصّة طالب التعليمية. جميع الحقوق محفوظة.{" "}
        </p>
      </div>

      <div className="flex gap-4 items-center">
        <p className="text-[16px] text-slate-900">طُوّر من قبل</p>
        <img src={perlaLogo} alt="perla logo" />
      </div>
    </div>
  );
};

export default Footer;
