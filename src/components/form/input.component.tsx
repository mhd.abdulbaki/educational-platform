import React from "react";

import { CloseEye, OpenEye } from "../../assets/icons/icon";
import { IconWrapper } from "..";
import { ErrorMessage, Field } from "formik";

interface InputComponentInterface {
  type: "text" | "email" | "password" | "number" | "tel";
  placeholder?: string;
  label?: string;
  label_for?: string;
  required?: boolean;
  ref?: React.MutableRefObject<unknown | any>;
  showPwd?: boolean;
  setShowPwd?: React.Dispatch<React.SetStateAction<boolean>> | any;
  name?: string;
}

const InputComponent: React.FC<InputComponentInterface> = ({
  type = "text",
  label,
  placeholder,
  label_for,
  required,
  ref,
  showPwd,
  setShowPwd,
  name,
}) => {
  return (
    <>
      <div className="md:mb-4 mb-2">
        <label
          htmlFor={label_for}
          className="block mb-2 text-sm font-medium text-slate-900 "
        >
          {label}
        </label>
        <div className="relative">
          <Field
            name={name}
            ref={ref}
            type={`${showPwd ? "text" : type}`}
            id={label_for}
            className=" border border-gray-300 text-slate-900 text-[16px] rounded-[5px] focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 outline-none "
            placeholder={placeholder}
            required={required}
          />
          {type === "password" ? (
            <>
              {showPwd ? (
                <IconWrapper onClick={() => setShowPwd((prv: boolean) => !prv)}>
                  <CloseEye />
                </IconWrapper>
              ) : (
                <IconWrapper onClick={() => setShowPwd((prv: boolean) => !prv)}>
                  <OpenEye />
                </IconWrapper>
              )}
            </>
          ) : null}
        </div>
        <ErrorMessage
          name={name as string}
          render={(msg) => <p className="text-sm text-red-700">{msg}</p>}
        />
      </div>
    </>
  );
};

export default InputComponent;
