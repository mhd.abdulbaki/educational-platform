import React, { useEffect, useState } from "react";

import PhoneInput from "react-phone-input-2";
import "react-phone-input-2/lib/style.css";

interface PhoneInputComponentInterface {
  label?: string;
  label_for?: string;
  placeholder?: string;
  setPhoneValue: React.Dispatch<React.SetStateAction<phoneValeType>>;
  phoneError?: boolean;
  errorMessage?: string;
}

const PhoneInputComponent: React.FC<PhoneInputComponentInterface> = ({
  label,
  label_for,
  placeholder,
  setPhoneValue,
  phoneError,
  errorMessage,
}) => {
  const [phone, setPhone] = useState<phoneInputType>({});

  const dialCodeLength = phone?.numberObject?.dialCode?.length;
  const phoneNumber = phone.number?.slice(dialCodeLength);

  useEffect(() => {
    setPhoneValue({ phoneNumber, dialCode: phone?.numberObject?.dialCode });
  }, [phone, phoneNumber, setPhoneValue]);

  return (
    <div dir="ltr" className="md:mb-4 mb-2">
      <label
        dir="rtl"
        htmlFor={label_for}
        className="block mb-2 text-sm font-medium text-slate-900 "
      >
        {label}
      </label>
      <PhoneInput
        placeholder={placeholder}
        enableSearch={true}
        country={"sy"}
        value={phone?.number}
        onChange={(number, numberObject) => setPhone({ number, numberObject })}
        containerStyle={{
          width: "100%",
        }}
        inputStyle={{
          width: "100%",
          height: "45px",
          borderRadius: "5px",
          fontSize: "16px",
        }}
      />
      {phoneError ? (
        <p dir="rtl" className="text-sm text-red-700">
          {errorMessage}
        </p>
      ) : null}
    </div>
  );
};

export default PhoneInputComponent;
