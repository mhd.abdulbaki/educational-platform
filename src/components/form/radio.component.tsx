import React from "react";

interface RadioComponentInterface {
  label: string;
  label_for: string;
  value: string;
  getValue: (value: string) => void | undefined;
}

const RadioComponent: React.FC<RadioComponentInterface> = ({
  label,
  label_for,
  value,
  getValue,
}) => {
  return (
    <>
      <input
        type="radio"
        id={label_for}
        name="hosting"
        value={value}
        className="hidden peer"
        required
        onChange={() => getValue(value)}
      />
      <label
        htmlFor={label_for}
        className="flex items-center justify-center md:w-32 md:py-4 md:px-3 p-2.5 text-slate-900 bg-white border rounded-[10px] cursor-pointer peer-checked:border-[#0056D2] peer-checked:bg-[#0056D2] peer-checked:text-white text-[16px] font-bold"
      >
        <span className="block">{label}</span>
      </label>
    </>
  );
};

export default RadioComponent;
