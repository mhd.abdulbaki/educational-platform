import React from "react";
import { ErrorMessage, Field } from "formik";

interface SelectComponentInterface {
  placeholder?: string;
  label?: string;
  label_for?: string;
  options: string[];
  name?: string;
}

const SelectComponent: React.FC<SelectComponentInterface> = ({
  label,
  label_for,
  placeholder,
  options,
  name,
}) => {
  return (
    <>
      <div className="md:mb-4 mb-2">
        <label
          htmlFor={label_for}
          className="block mb-2 text-sm font-medium text-slate-900 "
        >
          {label}
        </label>
        <Field
          as="select"
          name={name}
          id={label_for}
          className="bg-white border border-gray-300 text-slate-900  text-[16px] rounded-[5px] focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 outline-none"
        >
          <option selected>{placeholder}</option>
          {options.map((option) => (
            <option key={option} value={option}>
              {option}
            </option>
          ))}
        </Field>
        <ErrorMessage
          name={name as string}
          render={(msg) => <p className="text-sm text-red-700">{msg}</p>}
        />
      </div>
    </>
  );
};

export default SelectComponent;
