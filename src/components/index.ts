//main layout
import Footer from "./footer/footer";

//typography
import Heading from "./text/heading.component";
import Typography from "./text/typography.component";

//wrappers
import SectionWrapper from "./wrapper/section-wrapper.component";
import Wrapper from "./wrapper/wrapper.component";
import IconWrapper from "./wrapper/icon-wrapper.component";

//buttons
import Button from "./buttons/button.component";
import GooglePlayButton from "./buttons/google-play.component";
import AppleStoreButton from "./buttons/apple-store.component";

//modal
import Modal from "./modal/modal.component";

//form
import Input from "./form/input.component";
import Select from "./form/select.component";
import PhoneInput from "./form/phone-input.component";
import RadioButton from "./form/radio.component";
import FloatingInput from "./form/floating-input.component";

//Card
import Card from "./card/card.component";

import Carousel from "./carousel/carousel.component";

export {
  Footer,
  SectionWrapper,
  Typography,
  Heading,
  Button,
  Wrapper,
  IconWrapper,
  GooglePlayButton,
  AppleStoreButton,
  Modal,
  Input,
  FloatingInput,
  Select,
  PhoneInput,
  RadioButton,
  Card,
  Carousel,
};
