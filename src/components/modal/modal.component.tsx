import React, { ReactNode, useEffect } from "react";
import { Wrapper, Heading } from "..";

interface ModalComponentInterface {
  children?: ReactNode;
  title: string;
  showModal: boolean;
  setShowModal: () => unknown;
}

const ModalComponent: React.FC<ModalComponentInterface> = ({
  children,
  showModal,
  title,
  setShowModal,
}) => {
  useEffect(() => {
    if (showModal) {
      document.body.style.overflow = "hidden";
    }
    return () => {
      document.body.style.overflow = "unset";
    };
  }, [showModal]);
  return (
    <>
      {showModal ? (
        <>
          <div className="justify-center items-center flex overflow-x-hidden overflow-y-auto fixed inset-0  z-50">
            {/*content*/}
            <div className="border-0 rounded-lg shadow-lg relative flex flex-col w-full bg-white outline-none focus:outline-none md:w-[640px] mx-4 lg:mx-auto">
              {/*header*/}

              <Wrapper className="m-4">
                <Heading fontSize="text-[24px]" className="text-center">
                  {title}
                </Heading>

                <button
                  type="button"
                  className="absolute top-3 right-2.5 text-slate-900 bg-transparent hover:bg-gray-100  rounded-full text-2xl p-2 ml-auto inline-flex items-center"
                  onClick={setShowModal}
                >
                  <svg
                    aria-hidden="true"
                    className="w-5 h-5"
                    fill="currentColor"
                    viewBox="0 0 20 20"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      fill-rule="evenodd"
                      d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                      clip-rule="evenodd"
                    ></path>
                  </svg>
                  <span className="sr-only">Close modal</span>
                </button>
              </Wrapper>
              {children}
            </div>
          </div>

          <div className="opacity-75 fixed inset-0 z-40 bg-black"></div>
        </>
      ) : null}
    </>
  );
};

export default ModalComponent;
