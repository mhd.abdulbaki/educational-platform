import React, { ReactNode } from "react";

export interface Props {
  children: ReactNode | string;
  className?: string | undefined;
  fontSize?:
    | "text-xs"
    | "text-sm"
    | "text-md"
    | "text-lg"
    | "text-xl"
    | "text-2xl"
    | "text-3xl"
    | "text-4xl"
    | "text-5xl"
    | "text-6xl"
    | "text-7xl"
    | "text-8xl"
    | "text-9xl"
    | string;
  fontWeight?:
    | "font-bold"
    | "font-semibold"
    | "font-extrabold"
    | "font-light"
    | "font-thin"
    | "font-extralight"
    | "font-extralight"
    | "font-medium ";
  color?: string;
}

const Heading: React.FC<Props> = ({
  children,
  className,
  fontSize = "md:text-[32px] text-[28px]",
  fontWeight,
  color = "text-slate-900",
}) => {
  return (
    <h2
      className={`  capitalize font-bold  ${color}  ${className} ${fontSize} ${fontWeight}`}
    >
      {children}
    </h2>
  );
};

export default Heading;
