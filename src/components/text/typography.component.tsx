import React, { ReactNode } from "react";

interface Props {
  color?: string;
  fontSize?:
    | "text-xs"
    | "text-sm"
    | "text-md"
    | "text-lg"
    | "text-xl"
    | "text-2xl"
    | "text-3xl"
    | "text-4xl"
    | "text-5xl"
    | "text-6xl"
    | "text-7xl"
    | "text-8xl"
    | "text-9xl"
    | string;
  fontWeight?:
    | "font-bold"
    | "font-semibold"
    | "font-extrabold"
    | "font-light"
    | "font-thin"
    | "font-extralight"
    | "font-extralight"
    | "font-medium ";
  className?: string;
  textAlign?:
    | "text-start"
    | "text-center"
    | "text-end"
    | "text-right"
    | "text-left";
  children: ReactNode | string;
}

const Typography: React.FC<Props> = ({
  color,
  fontWeight,
  fontSize = "md:text-[24px] text-[18px]",
  textAlign,
  className,
  children,
}) => {
  return (
    <p
      className={`${className ? className : null} ${color ? color : "#000"} 
      ${fontWeight ? fontWeight : "font-normal"} ${fontSize} ${
        textAlign && textAlign
      } `}
    >
      {children}
    </p>
  );
};

export default Typography;
