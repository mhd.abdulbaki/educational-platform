import React, { ReactNode } from "react";

interface IconWrapperComponentInterface {
  children: ReactNode;
  className?: string;
  position?: "relative" | "absolute";
  onClick?: () => {} | React.Dispatch<React.SetStateAction<any>> | any;
}

const IconWrapperComponent: React.FC<IconWrapperComponentInterface> = ({
  children,
  className,
  position,
  onClick,
}) => {
  return (
    <span onClick={onClick} className={`${className} ${position}`}>
      {children}
    </span>
  );
};

export default IconWrapperComponent;
