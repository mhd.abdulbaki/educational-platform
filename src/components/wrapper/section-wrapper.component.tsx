import React, { ReactNode } from "react";

interface WrapperInterface {
  children?: ReactNode;
  height?: "h-screen" | "h-full" | string;
  className?: string | undefined;
  id?: string | undefined;
  margin?: string;
}

const SectionWrapperComponent: React.FC<WrapperInterface> = ({
  children,
  className,
  height,
  id,
  margin = "mx-auto mb-10",
}) => {
  return (
    <section
      id={id}
      className={`max-w-screen-3xl w-full ${margin}  ${height} ${className}`}
    >
      {children}
    </section>
  );
};

export default SectionWrapperComponent;
