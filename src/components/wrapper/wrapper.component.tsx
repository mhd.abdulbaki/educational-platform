import React, { ReactNode } from "react";

interface WrapperComponentInterface {
  className?: string;
  children?: ReactNode;
}
const WrapperComponent: React.FC<WrapperComponentInterface> = ({
  children,
  className,
}) => {
  return <div className={`${className}`}>{children}</div>;
};

export default WrapperComponent;
