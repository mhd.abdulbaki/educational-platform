export const COUNTRIES: string[] = [
  "الجمهورية العربية السورية",
  "جمهورية مصر العربية",
  "المملكة العربية السعودية",
  "دولة الكويت",
  "الامارات العربية المتحدة",
];

export const CITIES: string[] = [
  "دمشق",
  "حماة",
  "حلب",
  "حمص",
  "الاذقية",
  "ديرالزور",
  "درعا",
  "الرقة",
  "السويداء",
];

export const BENEFITS: string[] = [
  "  كادر تربوي تعليمي و تقني متكامل.",
  "   حلول متكاملة لدعم العملية التعليمية.",
  " نخبة من أفضل الأساتذة و المدرّبين.",
  " خبرة في إنشاء و صناعة و إدارة المحتوى التعليمي الرقمي.",
  " خبراء في تقديم الدعم التربوي و النفسي للطلاب.",
  " إتاحة إمكانية التّواصل و التّفاعل ما بين الطلاب و المدرسين.",
  " مراقبة أداء الطلاّب بهدف تحسين مخرجات العملية التّعليمية.",
];
