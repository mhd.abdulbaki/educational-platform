import { apiSlice } from "../../api/api.slice";

import { createSlice } from "@reduxjs/toolkit";

export const signInApiSlice = apiSlice.injectEndpoints({
  endpoints: (builder) => ({
    signin: builder.mutation({
      query: (body) => ({
        url: "/login",
        method: "POST",
        body: body,
      }),
    }),
  }),
});

export const { useSigninMutation } = signInApiSlice;

export const signinSlice = createSlice({
  name: "signin",
  initialState: {
    isOpen: false,
  },
  reducers: {
    openSignInModal: (state) => {
      state.isOpen = !state.isOpen;
    },
  },
});

export const { openSignInModal } = signinSlice.actions;

export default signinSlice.reducer;
