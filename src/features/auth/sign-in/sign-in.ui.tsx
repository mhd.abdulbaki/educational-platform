import React, { useState } from "react";
import {
  Button,
  Input,
  Modal,
  PhoneInput,
  Typography,
} from "../../../components";
import { AppDispatch, RootState } from "../../../app/store";
import { useDispatch, useSelector } from "react-redux";
import { openSignInModal } from "./sign-in.slice";
import { openSignUpModal } from "../sign-up/sign-up.slice";

import { Formik, Form } from "formik";
import { signInValidationSchema } from "./sign-in.validation";

//Query
import { useSigninMutation } from "./sign-in.slice";

const SignInUI: React.FC = () => {
  const dispatch: AppDispatch = useDispatch();
  const { isOpen } = useSelector((state: RootState) => state.signin);

  const [signIn, { isSuccess }] = useSigninMutation();

  const createNewAccountHandler = () => {
    //close sign in modal
    dispatch(openSignInModal());
    // open sign up modal
    dispatch(openSignUpModal());
  };
  const [phoneValue, setPhoneValue] = useState<phoneValeType>({});
  const [phoneError, setPhoneError] = useState(false);

  const [showPwd, setShowPwd] = useState<boolean>(false);

  const initialValues = { password: "", Device_id: "455" };

  const onSubmitHandler = (
    data: typeof initialValues,
    { resetForm }: { resetForm: any }
  ) => {
    if (
      phoneValue.phoneNumber === "" ||
      !phoneValue.phoneNumber ||
      !phoneValue.dialCode
    ) {
      setPhoneError(true);
      return;
    }
    setPhoneError(false);
    data = Object.assign(
      { country_code: phoneValue.dialCode, phone: phoneValue.phoneNumber },
      data
    );

    signIn(data);

    isSuccess && resetForm();

    dispatch(openSignInModal());
  };

  return (
    <div>
      <Modal
        title="تسجيل الدخول"
        setShowModal={() => dispatch(openSignInModal())}
        showModal={isOpen}
      >
        <Formik
          initialValues={initialValues}
          onSubmit={onSubmitHandler}
          validationSchema={signInValidationSchema}
        >
          {() => (
            <Form className="lg:px-12 px-4">
              <PhoneInput
                label="رقم الهاتف المحمول"
                label_for="phone_number"
                placeholder="أدخل رقم هاتفك"
                setPhoneValue={setPhoneValue}
                phoneError={phoneError}
                errorMessage="يرجى ادخال رقم الهاتف"
              />

              <Input
                type="password"
                placeholder="أدخل كلمة المرور"
                label="كلمة المرور"
                label_for="password"
                showPwd={showPwd}
                setShowPwd={setShowPwd}
                name="password"
              />
              <Button
                bgColor={"bg-[#0056D2] hover:bg-[#1673f5]"}
                color={"text-white"}
                padding=" py-2 px-4"
                className=" text-[18px] w-full mt-12 mb-4"
                type="submit"
              >
                <span> تسجيل الدخول</span>
              </Button>
            </Form>
          )}
        </Formik>

        <Typography
          textAlign="text-center"
          className="mb-8 flex justify-center items-center "
          fontSize="md:text-[18px] text-[16px]"
        >
          <span> ليس لديك حساب؟</span>
          <Button
            onClick={createNewAccountHandler}
            type={"button"}
            color={"text-[#0056D2]"}
          >
            إنشاء حساب
          </Button>
        </Typography>
      </Modal>
    </div>
  );
};

export default SignInUI;
