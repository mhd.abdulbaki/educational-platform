import { apiSlice } from "../../api/api.slice";

import { createSlice } from "@reduxjs/toolkit";

export const signUpApiSlice = apiSlice.injectEndpoints({
  endpoints: (builder) => ({
    signUp: builder.mutation({
      query: (body) => ({
        url: "/register",
        method: "POST",
        body: body,
      }),
    }),
  }),
});

export const { useSignUpMutation } = signUpApiSlice;

export const signupSlice = createSlice({
  name: "signup",
  initialState: {
    isOpen: false,
  },
  reducers: {
    openSignUpModal: (state) => {
      state.isOpen = !state.isOpen;
    },
  },
});

export const { openSignUpModal } = signupSlice.actions;

export default signupSlice.reducer;
