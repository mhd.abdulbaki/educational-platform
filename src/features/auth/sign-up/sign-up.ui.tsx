import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Form, Formik } from "formik";
import { signUpValidationSchema } from "./sign-up.validation";

import { openSignUpModal } from "./sign-up.slice";
import { openSignInModal } from "../sign-in/sign-in.slice";
import { RootState, AppDispatch } from "../../../app/store";

//Components
import {
  Modal,
  Input,
  Select,
  Button,
  Typography,
  PhoneInput,
} from "../../../components";

//CONSTANTS
import { COUNTRIES, CITIES } from "../../../constants";

//Query
import { useSignUpMutation } from "./sign-up.slice";

const SignUpUI: React.FC = () => {
  const dispatch: AppDispatch = useDispatch();
  const { isOpen } = useSelector((state: RootState) => state.signup);

  const [signUp, { isSuccess }] = useSignUpMutation();

  const alreadyHaveAnAccountHandler = () => {
    // close sign up modal
    dispatch(openSignUpModal());
    //open sign in modal
    dispatch(openSignInModal());
  };

  const [phoneValue, setPhoneValue] = useState<phoneValeType>({});
  const [phoneError, setPhoneError] = useState(false);

  const [showPwd1, setShowPwd1] = useState<boolean>(false);
  const [showPwd2, setShowPwd2] = useState<boolean>(false);

  const initialValues = {
    full_name: "",
    father_name: "",
    country: "",
    state: "",
    location: "",
    password: "",
    confirm_password: "",
    Device_id: "455",
  };

  const onSubmitHandler = (
    data: typeof initialValues,
    { resetForm }: { resetForm: any }
  ) => {
    if (
      phoneValue.phoneNumber === "" ||
      !phoneValue.phoneNumber ||
      !phoneValue.dialCode
    ) {
      setPhoneError(true);
      return;
    }
    setPhoneError(false);
    data = Object.assign(
      { country_code: phoneValue.dialCode, phone: phoneValue.phoneNumber },
      data
    );
    signUp(data);
    isSuccess && resetForm();
    dispatch(openSignUpModal());
  };

  return (
    <div>
      <Modal
        title="إنشاء حساب"
        setShowModal={() => dispatch(openSignUpModal())}
        showModal={isOpen}
      >
        <Formik
          initialValues={initialValues}
          onSubmit={onSubmitHandler}
          validationSchema={signUpValidationSchema}
        >
          {({ values, isSubmitting }) => (
            <Form className="lg:px-12 px-4">
              <Input
                type="text"
                placeholder={"أدخل الاسم والكنية"}
                label="الاسم الكامل"
                label_for="full_name"
                name="full_name"
              />

              <Input
                type="text"
                placeholder={"أدخل اسم الأب"}
                label="اسم الأب"
                label_for="father_name"
                name="father_name"
              />

              <Select
                label="الدولة"
                label_for="country"
                placeholder="اختر دولتك"
                options={COUNTRIES}
                name="country"
              />

              {/* <Select
                label="المدينة"
                label_for="cities"
                placeholder="اختر مدينتك"
                options={CITIES}
             /> */}

              <PhoneInput
                label="رقم الهاتف المحمول"
                label_for="phone_number"
                placeholder="أدخل رقم هاتفك"
                setPhoneValue={setPhoneValue}
                phoneError={phoneError}
                errorMessage="يرجى ادخال رقم الهاتف"
              />

              <Input
                type="password"
                placeholder="أدخل كلمة المرور"
                label="كلمة المرور"
                label_for="password"
                showPwd={showPwd1}
                setShowPwd={setShowPwd1}
                name="password"
              />

              <Input
                type="password"
                placeholder="أدخل تأكيد كلمة المرور"
                label=" تأكيد كلمة المرور"
                label_for="confirm_password"
                showPwd={showPwd2}
                setShowPwd={setShowPwd2}
                name="confirm_password"
              />

              <Button
                bgColor={"bg-[#0056D2] hover:bg-[#1673f5]"}
                color={"text-white"}
                padding=" py-2 px-4"
                className=" text-[18px] w-full mt-12 mb-2"
                type="submit"
              >
                <span> إنشاء حساب</span>
              </Button>
            </Form>
          )}
        </Formik>

        <Typography
          textAlign="text-center"
          className="mb-4 flex justify-center items-center"
          fontSize="md:text-[18px] text-[16px]"
        >
          <span>لديك حساب مسبقا على طالب؟</span>
          <Button
            onClick={alreadyHaveAnAccountHandler}
            type={"button"}
            color={"text-[#0056D2]"}
          >
            تسجيل الدخول
          </Button>
        </Typography>
      </Modal>
    </div>
  );
};

export default SignUpUI;
