import * as Yup from "yup";

export const signUpValidationSchema = Yup.object({
  full_name: Yup.string().required("يرجى ادخال الاسم الكامل").min(4).max(128),
  father_name: Yup.string().required("يرجى ادخال اسم الاب").min(1).max(32),
  country: Yup.string().required("يرجى اختيار الدولة").min(4).max(128),
  password: Yup.string()
    .min(8)
    .max(32)
    .matches(
      /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*^#?&-._!"`'#%&,:;<>=@{}~\$\(\)\*\+\/\\\?\[\]\^\|])[A-Za-z\d@$#!^%*?&-._!"`'#%&,:;<>=@{}~\$\(\)\*\+\/\\\?\[\]\^\|]{8,32}$/,
      "يجب أن يحتوي على 8 أحرف على الأقل ، وحرف كبير واحد ، وحرف صغير واحد ، ورقم واحد ، وحرف خاص واحد"
    )
    .required("يرجى ادخال كلمة المرور"),
  confirm_password: Yup.string()
    .required("يرجى ادخال تأكيد كلمة المرور")
    .oneOf([Yup.ref("password")], "يجب أن تتطابق كلمات المرور"),
});
