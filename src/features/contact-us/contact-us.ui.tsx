import React from "react";
import {
  SectionWrapper,
  Wrapper,
  FloatingInput,
  Button,
  Typography,
  AppleStoreButton,
  GooglePlayButton,
  IconWrapper,
  Heading,
} from "../../components";

// Image and icons
import smallCircle from "../../assets/s-c.png";
import bigCircle from "../../assets/b-c.png";
import Facebook from "../../assets/Facebook.png";
import WhatsApp from "../../assets/WhatsApp.png";
import Telegram from "../../assets/Telegram.png";
import Youtube from "../../assets/Youtube.png";
import { Envelop, Phone } from "../../assets/icons/icon";

const ContactUsUI: React.FC = () => {
  return (
    <SectionWrapper margin="mx-auto md:mb-40 mb-16 md:px-24 px-8 md:mt-40">
      <Heading>تواصل معنا</Heading>
      <div className="grid lg:grid-cols-7 grid-cols-1 mt-8">
        <Wrapper className="lg:col-span-2 bg-[#0056D2] w-full h-full relative overflow-hidden rounded-lg flex flex-col justify-between p-8 order-last lg:order-first lg:mt-0 mt-16">
          <div className="flex flex-col gap-6">
            <Typography fontSize="text-[18px]" color="text-white">
              في حال لديك أسئلة أو استفسارات فقط تواصل معنا
            </Typography>
            <Typography
              fontSize="text-[18px]"
              color="text-white"
              className="flex gap-2 items-center"
            >
              <Phone /> <span dir="ltr">(+963) 935 839 150</span>
            </Typography>
            <Typography
              fontSize="text-[18px]"
              color="text-white"
              className="flex gap-2 items-center"
            >
              <Envelop /> <span>info@taleb.io</span>
            </Typography>
          </div>

          <Wrapper className="my-14">
            <Typography
              fontSize="text-[16px]"
              color="text-white"
              className="flex gap-2 items-center"
            >
              حمل التطبيق الآن
            </Typography>
            <div className="flex flex-col xl:flex-row justify-center md:gap-8 gap-4 my-4">
              <AppleStoreButton />
              <GooglePlayButton />
            </div>
            <Typography
              fontSize="text-[16px]"
              color="text-white"
              className="text-center"
            >
              يمكنك تحميل التطبيق بشكل مباشر للأندرويد من هذا الرابط
            </Typography>
          </Wrapper>

          <Wrapper className="flex justify-evenly items-center flex-row-reverse gap-2">
            <IconWrapper className="w-16 h-16">
              <img src={Facebook} alt="Facebook" />
            </IconWrapper>
            <IconWrapper className="w-16 h-16">
              <img src={WhatsApp} alt="WhatsApp" />
            </IconWrapper>
            <IconWrapper className="w-16 h-16">
              <img src={Telegram} alt="Telegram" />
            </IconWrapper>

            <IconWrapper className="w-16 h-16">
              <img src={Youtube} alt="Youtube" />
            </IconWrapper>
          </Wrapper>

          <div className="absolute right-20 bottom-14">
            <img src={bigCircle} alt="bigCircle" />
          </div>
          <div className="absolute -bottom-20 -right-20">
            <img src={smallCircle} alt="smallCircle" />
          </div>
        </Wrapper>

        <Wrapper className="lg:col-span-5 ">
          <form className="flex flex-col lg:px-32">
            <div className="grid md:grid-cols-2 md:gap-6 md:mb-10 mb-2">
              <div className="relative z-0 w-full mb-6 group">
                <FloatingInput
                  type={"text"}
                  label="الاسم الكامل"
                  label_for="full_name"
                  name="full_name"
                  placeholder="اكتب الاسم هنا"
                />
              </div>
              <div className="relative z-0 w-full mb-6 group">
                <FloatingInput
                  type="text"
                  label="موضوع الرسالة"
                  label_for="subject"
                  name="subject"
                  placeholder="اختر موضوع رسالتك"
                />
              </div>
            </div>

            <div className="grid md:grid-cols-2 md:gap-6 md:mb-10 mb-2">
              <div className="relative z-0 w-full mb-6 group">
                <FloatingInput
                  type="number"
                  label="رقم الهاتف المحمول"
                  label_for="phone"
                  name="phone"
                  placeholder="ادخل رقم الهاتف المحمول"
                />
              </div>
              <div className="relative z-0 w-full mb-6 group">
                <FloatingInput
                  type="email"
                  label="البريد الإلكتروني"
                  label_for="email"
                  name="email"
                  placeholder="أدخل عنوان البريد الالكتروني"
                />
              </div>
            </div>

            <div className="relative z-0 w-full group md:mb-10 mb-2">
              <FloatingInput
                type="email"
                label="الرسالة"
                label_for="message"
                name="message"
                placeholder="اكتب رسالتك هنا"
              />
            </div>

            <Button
              bgColor={"bg-[#0056D2] hover:bg-[#1673f5]"}
              color={"text-white"}
              padding=" py-2.5 px-8"
              className=" text-[18px]  md:mt-10 mb-4 w-fit self-end"
              type="submit"
            >
              ارسال
            </Button>
          </form>
        </Wrapper>
      </div>
    </SectionWrapper>
  );
};

export default ContactUsUI;
