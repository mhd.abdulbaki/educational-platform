import { apiSlice } from "../api/api.slice";

export const heroApiSlice = apiSlice.injectEndpoints({
  endpoints: (builder) => ({
    getHero: builder.query({
      query: () => ({
        url: "/get_hero_section",
        method: "GET",
      }),
      providesTags: ["Hero"],
    }),
  }),
});

export const { useGetHeroQuery } = heroApiSlice;
