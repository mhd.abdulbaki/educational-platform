import React from "react";
import {
  SectionWrapper,
  Wrapper,
  Typography,
  Button,
  Heading,
  AppleStoreButton,
  GooglePlayButton,
} from "../../components";

import heroImage from "../../assets/image1.png";

// Api
import { useGetHeroQuery } from "./hero.slice";

const HeroUi: React.FC = () => {
  const {
    data: heroData,
    isLoading,
    isSuccess,
    isError,
    error,
  } = useGetHeroQuery({});

  return (
    <SectionWrapper className="grid lg:grid-cols-2 grid-cols-1 mt-8">
      <Wrapper className="flex flex-col justify-center lg:mr-24 lg:mb-0 mb-8 mx-8">
        <Heading>تعلّم بلا حدود</Heading>
        <Typography color="text-[#333]" className="mt-2 mb-8">
          نقدم لطلابنا فرصة التعلّم بدون قيود في أي وقت و أي مكان مع نخبة من
          أفضل المدرّسين و الخبراء في المجال التعليمي و التربوي. نسعى لنقدّم
          العلم و المعرفة من خلال أحدث الوسائل التكنولوجية لنصل إلى كل طالب.
        </Typography>
        <Button
          type={"button"}
          bgColor={"bg-[#0056D2]"}
          color={"text-white"}
          padding="md:py-3 px-16 py-3"
          className=" w-fit text-[18px] mx-auto lg:mx-0"
        >
          سجّل معنا
        </Button>
      </Wrapper>

      <Wrapper>
        <div className="">
          <img
            src={`${heroImage}`}
            alt="hero student"
            className="object-fill h-full"
          />
        </div>

        <div className="flex justify-center md:gap-8 gap-4 my-4">
          <AppleStoreButton />
          <GooglePlayButton />
        </div>
      </Wrapper>
    </SectionWrapper>
  );
};

export default HeroUi;
