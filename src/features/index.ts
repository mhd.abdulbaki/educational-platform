import NavbarUI from "./navbar/navbar.ui";
import HeroUI from "./hero/hero.ui";
import SignUpUI from "./auth/sign-up/sign-up.ui";
import SignInUI from "./auth/sign-in/sign-in.ui";
import SubjectsUI from "./subjects/subjects.ui";
import WhyUsUI from "./why-us/why-us.ui";
import OurPartnersUI from "./our-partners/our-partners.ui";
import ContactUsUI from "./contact-us/contact-us.ui";

export {
  HeroUI,
  SignUpUI,
  SignInUI,
  SubjectsUI,
  WhyUsUI,
  OurPartnersUI,
  NavbarUI,
  ContactUsUI,
};
