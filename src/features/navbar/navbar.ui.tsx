import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { openSignUpModal } from "../auth/sign-up/sign-up.slice";
import { openSignInModal } from "../auth/sign-in/sign-in.slice";

import { Button, IconWrapper, Wrapper } from "../../components";
import { AppDispatch } from "../../app/store";

import { BergerMenu, CloseX, Search } from "../../assets/icons/icon";
import logo from "../../assets/logo.png";

const NavbarUI: React.FC = () => {
  const dispatch: AppDispatch = useDispatch();

  const [openMenu, setOpenMenu] = useState(false);

  useEffect(() => {
    if (openMenu) {
      document.body.style.overflow = "hidden";
    } else {
      document.body.style.overflow = "unset";
    }
  }, [openMenu]);

  return (
    <>
      <nav className="bg-white border-gray-200">
        <div className="md:max-w-screen-3xl flex flex-wrap items-center justify-between mx-auto p-4">
          <div className="flex justify-between items-center w-full lg:w-[60%] h-16">
            <a
              href="https://flowbite.com/"
              className="flex items-center ml-4 h-14 justify-center"
            >
              <img src={logo} className="h-full" alt="Logo" />
            </a>

            <div className="ml-8 flex-grow relative">
              <input
                type="text"
                id="search-navbar"
                className="block w-full px-2 py-2.5 pl-10 text-sm text-gray-900 border border-[#0056D2] rounded-[5px] focus:border-blue-500 outline-none"
                placeholder="ماذا تريد أن تتعلم؟"
              />
              <IconWrapper
                position="absolute"
                className=" bg-[#0056D2] top-1/2 left-7 transform -translate-x-1/2 -translate-y-1/2 cursor-pointer px-2 py-1 rounded"
              >
                <Search />
              </IconWrapper>
            </div>

            <button
              onClick={() => setOpenMenu(true)}
              className="inline-flex items-center p-2 ml-3 text-md text-slate-900 rounded-lg lg:hidden hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-gray-200"
            >
              <span className="sr-only">Open main menu</span>
              <BergerMenu />
            </button>

            <ul className="lg:flex hidden flex-grow items-center p-4 md:p-0 mt-4 font-medium   ">
              <li>
                <a
                  href="/#"
                  className="block py-2 pl-3 pr-4 text-[#0056D2]"
                  aria-current="page"
                >
                  الرئيسة
                </a>
              </li>
              <li>
                <a
                  href="/#"
                  className="block py-2 pl-3 pr-4 text-slate-900  "
                  aria-current="page"
                >
                  هول منصة الطالب
                </a>
              </li>
            </ul>
          </div>

          <Wrapper className="lg:flex hidden ">
            <Button
              type={"button"}
              bgColor={""}
              color={"text-[#0056D2]"}
              padding=" py-2 px-4"
              className=" w-fit text-[18px] "
              onClick={() => dispatch(openSignInModal())}
            >
              تسجيل الدخول
            </Button>
            <Button
              type={"button"}
              bgColor={"bg-[#0056D2]"}
              color={"text-white"}
              padding=" py-2 px-4"
              className=" w-fit text-[18px] "
              onClick={() => dispatch(openSignUpModal())}
            >
              إنشاء حساب
            </Button>
          </Wrapper>
        </div>

        {/* Side Nav */}
        <div>
          <div
            className={`bg-white absolute flex-col items-center lg:hidden self-end z-40  font-bold h-[100vh] w-[60%] top-0 drop-shadow-md transition delay-150 duration-300 ease-in-out ${
              openMenu ? "left-0 " : "-left-[1000px]"
            }`}
          >
            <IconWrapper
              className="flex justify-center my-4 cursor-pointer rounded-full w-fit  hover:bg-gray-100 mx-auto p-4"
              onClick={() => setOpenMenu(false)}
            >
              <CloseX />
            </IconWrapper>

            <ul className="flex flex-col items-center p-4 md:p-0 mt-4 font-medium   ">
              <li>
                <a
                  href="/#"
                  className="block py-2 pl-3 pr-4 text-[#0056D2]"
                  aria-current="page"
                >
                  الرئيسة
                </a>
              </li>
              <li>
                <a
                  href="/#"
                  className="block py-2 pl-3 pr-4 text-slate-900  "
                  aria-current="page"
                >
                  هول منصة الطالب
                </a>
              </li>
            </ul>

            <Wrapper className="flex flex-col items-center mt-28">
              <Button
                type={"button"}
                bgColor={""}
                color={"text-[#0056D2]"}
                padding=" py-2 px-4"
                className=" w-fit text-[18px] mb-8"
                onClick={() => {
                  setOpenMenu(false);
                  dispatch(openSignInModal());
                }}
              >
                تسجيل الدخول
              </Button>
              <Button
                type={"button"}
                bgColor={"bg-[#0056D2]"}
                color={"text-white"}
                padding=" py-2 px-4"
                className=" w-fit text-[18px] "
                onClick={() => {
                  dispatch(openSignUpModal());
                  setOpenMenu(false);
                }}
              >
                إنشاء حساب
              </Button>
            </Wrapper>
          </div>
        </div>
        <div
          className={`absolute top-0 left-0 w-screen h-screen bg-black/70 z-30 ${
            openMenu ? "flex" : "hidden"
          }`}
        />
      </nav>
    </>
  );
};

export default NavbarUI;
