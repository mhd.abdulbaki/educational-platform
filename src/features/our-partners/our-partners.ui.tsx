import React from "react";
import { SectionWrapper, Carousel, Heading, Wrapper } from "../../components";
import { SwiperSlide } from "swiper/react";

import microsoft from "../../assets/microsoft.png";
import coursera from "../../assets/coursera.png";
import alko from "../../assets/alko.png";
import udemy from "../../assets/udemy.png";

const OurPartnersUI: React.FC = () => {
  const IMAGES: string[] = [
    microsoft,
    coursera,
    alko,
    udemy,
    coursera,
    alko,
    udemy,
    microsoft,
  ];

  return (
    <SectionWrapper className="mb-24 mt-4">
      <Heading className=" md:px-24 px-8">شركاؤنا</Heading>
      <Wrapper className="mt-10 mx-auto">
        <Carousel
          pagination={false}
          breakpoints={{
            320: { slidesPerView: 2 },
            360: { slidesPerView: 3 },
            900: { slidesPerView: 4 },
            1200: { slidesPerView: 5 },
          }}
        >
          {IMAGES.map((img, idx) => (
            <SwiperSlide key={`IMG-${idx}-slide`}>
              <div className="w-28 h-28  border-2 border-gray-50 rounded-full flex justify-center items-center  overflow-hidden">
                <img src={img} alt="" className="h-[70%]" />
              </div>
            </SwiperSlide>
          ))}
        </Carousel>
      </Wrapper>
    </SectionWrapper>
  );
};

export default OurPartnersUI;
