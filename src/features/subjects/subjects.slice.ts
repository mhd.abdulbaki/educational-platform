import { apiSlice } from "../api/api.slice";

export const subjectsApiSlice = apiSlice.injectEndpoints({
  endpoints: (builder) => ({
    getSubject: builder.query({
      query: ({ search }) => ({
        url: `/log_in?search=${search}`,
        method: "GET",
      }),
      providesTags: ["Subject"],
    }),
  }),
});

export const { useLazyGetSubjectQuery, useGetSubjectQuery } = subjectsApiSlice;
