import React, { useEffect } from "react";
import {
  Card,
  Carousel,
  Heading,
  RadioButton,
  SectionWrapper,
  Wrapper,
} from "../../components";

import { SwiperSlide } from "swiper/react";

import { useLazyGetSubjectQuery } from "./subjects.slice";

import ArabicBook from "../../assets/arabic-book.png";

const SubjectsUI: React.FC = () => {
  const [trigger, { data }] = useLazyGetSubjectQuery();

  const getSubjectsValue = (value: string) => {
    trigger(value);
  };

  useEffect(() => {
    trigger("");
  }, []);

  return (
    <SectionWrapper margin="mx-auto md:mb-40 mb-16 md:px-24 px-8">
      <Heading>المناهج الدّراسية</Heading>
      <ul className="flex md:gap-4 my-8 gap-2 ">
        <li>
          <RadioButton
            label={"تاسع"}
            label_for={"9th"}
            value={"تاسع"}
            getValue={getSubjectsValue}
          />
        </li>
        <li>
          <RadioButton
            label={"بكالوريا علمي"}
            label_for={"11th"}
            value={"بكالوريا علمي"}
            getValue={getSubjectsValue}
          />
        </li>
        <li>
          <RadioButton
            label={"بكالوريا أدبي"}
            label_for={"12th"}
            value={"بكالوريا أدبي"}
            getValue={getSubjectsValue}
          />
        </li>
      </ul>

      <Wrapper>
        {data ? (
          <Carousel
            pagination={{
              clickable: true,
            }}
            breakpoints={{
              440: { slidesPerView: 1 },
              640: { slidesPerView: 2 },
              900: { slidesPerView: 3 },
              1200: { slidesPerView: 4 },
            }}
          >
            {data?.map((subject: subjectsType) => (
              <SwiperSlide>
                <Card
                  imageSrc={ArabicBook}
                  imageAlt={subject.title}
                  title={subject.title}
                  des={subject.description}
                  stars={subject.stars}
                  reviews={subject.reviews}
                  points={subject.points}
                />
              </SwiperSlide>
            ))}
          </Carousel>
        ) : (
          <Heading className="text-center my-20 " color="text-gray-500">
            لا يوجد بيانات لعرضها
          </Heading>
        )}
      </Wrapper>
    </SectionWrapper>
  );
};

export default SubjectsUI;
