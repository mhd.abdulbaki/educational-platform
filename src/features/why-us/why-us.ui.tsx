import React from "react";
import { Heading, SectionWrapper, Typography, Wrapper } from "../../components";
import image from "../../assets/image2.png";
import { BENEFITS } from "../../constants";

const WhyUsUI: React.FC = () => {
  return (
    <SectionWrapper className="grid lg:grid-cols-2 grid-cols-1 md:px-24 px-8 mb-24 mt-4 gap-4">
      <Wrapper className="mb-8">
        <Heading>لماذا نحن؟</Heading>
        <Typography color="text-[#333]" className="mt-2 mb-8 mx-auto">
          تمتاز منصة طالب التّعليمية بباقة كبيرة من المميزات التي تجعلها وجهة
          الباحثين عن التعليم عن بُعد وتحصيله على يد أكاديميين وتربويين معتمدين
          من خلال تقديم:
        </Typography>
        <ul className="list-disc md:mr-16 mr-8">
          {BENEFITS.map((be) => (
            <li key={be} className="text-[#33] md:text-[24px] mb-4">
              {be}
            </li>
          ))}
        </ul>
      </Wrapper>

      <Wrapper className="w-full flex justify-center items-center">
        <div className="flex justify-center items-center relative bg-slate-400 ">
          <div className="md:w-[138px] w-14 h-14 md:h-[138px] rounded-xl md:rounded-3xl bg-[#0056D2] absolute -top-4 -left-4 z-10" />

          <img src={image} alt="" className="z-20 w-full h-full object-fill" />

          <div className="md:w-[231px] w-20 h-20 md:h-[231px] rounded-xl md:rounded-3xl bg-[#0056D2] absolute -bottom-4 -right-4 z-10" />

          <div className="absolute z-30 flex justify-center items-center w-16 h-16 rounded-full bg-white">
            <svg
              className="w-6 h-6 text-[#0056D2] "
              aria-hidden="true"
              xmlns="http://www.w3.org/2000/svg"
              fill="currentColor"
              viewBox="0 0 14 16"
            >
              <path d="M0 .984v14.032a1 1 0 0 0 1.506.845l12.006-7.016a.974.974 0 0 0 0-1.69L1.506.139A1 1 0 0 0 0 .984Z" />
            </svg>
          </div>
        </div>
      </Wrapper>
    </SectionWrapper>
  );
};

export default WhyUsUI;
