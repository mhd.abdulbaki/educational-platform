type phoneInputType = {
  number?: string;
  numberObject?: {
    countryCode?: string;
    dialCode?: string;
    format?: string;
    name?: string;
  };
};

type phoneValeType = {
  phoneNumber?: string;
  dialCode?: string;
};

type subjectsType = {
  imageSrc: string;
  title: string;
  description: string;
  stars: number;
  reviews: number;
  points: number;
};
