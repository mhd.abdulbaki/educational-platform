/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{html,ts,tsx}"],
  theme: {
    theme: {
      Montserrat: ["Montserrat", " sans-serif"],
      extend: {},
    },
  },
  plugins: [require("tailwindcss-rtl")],
};
